/* Copyright (c) 2012, Jean-Benoist Leger <leger@jblg.fr>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */



/* time stamp of key creation */
#define timestamp ((unsigned int)1342103578)

/* size of the key */
#define Tbits 4096

/* Target key */
/* For the key E67A 2102 E00B D185 3BD2  E96B 196A 3B2C 7C48 5DC1 */
/* fistly fpr (without short id) */
#define TARGET_FPR {0xE6,0x7A,0x21,0x02,0xE0,0x0B,0xD1,0x85,0x3B,0xD2,0xE9,0x6B,0x19,0x6A,0x3B,0x2C}
/* secondly short id */
#define TARGET_ID {0x7C,0x48,0x5D,0xC1}


/* Customization only to change generation model !!! */

/* Generate a RSA n/p/q for how many number of e tested ? */
#define RESTART_FROM_KEY 5000000000
#define DISPLAY_EACH 10000000

/* probability of looking the i hexa number */
#define VIEW(i) pow(.5,((double)i)/4.0)

/* probability for looking keys
 * 
 * computed with a penalty law .5^(i/4.),
 * and penalty inside blocks .92265 (compuded with training)
 *
 * computation with GNU octave:
 * pascend=.5.^((0:39)/4);
 * pdescend=.5.^((39:-1:0)/4);
 * p1=max(pascend,pdescend);
 * p2=kron(ones(1,10),[1,.92265,.92265,1]);
 * p=p1.*p2;
 * p(1:32)
 */

#define LOOKING {1.000000, 0.775853, 0.652412, 0.594604, 0.500000, 0.387927, 0.326206, 0.297302, 0.250000, 0.193963, 0.163103, 0.148651, 0.125000, 0.096982, 0.081552, 0.074325, 0.062500, 0.048491, 0.040776, 0.037163, 0.037163, 0.040776, 0.048491, 0.062500, 0.062500, 0.048491, 0.040776, 0.037163, 0.037163, 0.040776, 0.048491, 0.062500}

/* probability of confusionning hexa number (times 1e6) (comuted with
 * trainig
 *                      0       1       2       3       4       5       6       7       8       9       A       B       C       D       E       F */
#define CONFUSE_0 {883379,    210,    365,    241,    244,      1,    216,      1,  13853,      1,      1,  14021,  24855,   2033,  13369,    410}
#define CONFUSE_1 {   210, 972608,  11881,  15873,      1,  18261,      1,      1,      1,      1,      1,  12645,    156,  13720,    272,    375}
#define CONFUSE_2 {   365,  11881, 906808,  15491,    194,    223,    516,    411,    344,  24259,      1,      1,      1,    147,  20789,    163}
#define CONFUSE_3 {   241,  15873,  15491, 878055,    256,  17637,    680,    541,    454,  40541,      1,  14452,    178,   2129,    311,    644}
#define CONFUSE_4 {   244,      1,    194,    256, 906348,      1,    691,    550,  28802,  28117,  30273,    241,      1,      1,  18659,    218}
#define CONFUSE_5 {     1,  18261,    223,  17637,      1, 858711,      1,  27778,    529,    501,    285,  14096,    624,    452,   1816,  30030}
#define CONFUSE_6 {   216,      1,    516,    680,    691,      1, 864490,   1786,   1429,  40541,      1,    640,  13483,  12195,      1,    579}
#define CONFUSE_7 {     1,      1,    411,    541,    550,  27778,   1786, 870093,  14610,  32248,  14336,   1187,  10725,  30488,      1,  13206}
#define CONFUSE_8 { 13853,      1,    344,    454,  28802,    529,   1429,  14610, 837551,  27220,    440,  14499,    642,  12195,   1681,  13900}
#define CONFUSE_9 {     1,      1,  24259,  40541,  28117,    501,  40541,  32248,  27220, 895909,    208,      1,      1,      1,    265,  12783}
#define CONFUSE_A {     1,      1,      1,      1,  30273,    285,      1,  14336,    440,    208, 939882,      1,    173,    188,    302,    208}
#define CONFUSE_B { 14021,  12645,      1,  14452,    241,  14096,    640,   1187,  14499,      1,      1, 831588,  11068, 148708,  19315,  38725}
#define CONFUSE_C { 24855,    156,      1,    178,      1,    624,  13483,  10725,    642,      1,    173,  11068, 870723,  22335,   2423,  21409}
#define CONFUSE_D {  2033,  13720,    147,   2129,      1,    452,  12195,  30488,  12195,      1,    188, 148708,  22335, 723379,   2391,   3461}
#define CONFUSE_E { 13369,    272,  20789,    311,  18659,   1816,      1,      1,   1681,    265,    302,  19315,   2423,   2391, 754710,  91945}
#define CONFUSE_F {   410,    375,    163,    644,    218,  30030,    579,  13206,  13900,  12783,    208,  38725,  21409,   3461,  91945, 870709}


