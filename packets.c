/* Copyright (c) 2012, Jean-Benoist Leger <leger@jblg.fr>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include "all.h"

int get_pub_packet(unsigned char * str, BIGNUM* n, BIGNUM* e, BIGNUM* d, int complet)
{

    unsigned int lBe=(unsigned int)BN_num_bytes(e);
    unsigned int lbe=(unsigned int)BN_num_bits(e);

    //header
    str[1]=((10+Tbytes+lBe)>>8);
    str[2]=(10+Tbytes+lBe);
    if(complet)
    {
        str[0]=0x99;

        //version
        str[3]=4;

        //time
        str[4]=(unsigned char)(timestamp>>24);
        str[5]=(unsigned char)(timestamp>>16);
        str[6]=(unsigned char)(timestamp>>8);
        str[7]=(unsigned char)timestamp;

        //algo
        str[8]=1;

        //MPI n
        str[9]=(unsigned char)(Tbits>>8);
        str[10]=(unsigned char)(Tbits);
        BN_bn2bin(n,str+11);
    }

    //MPI e
    str[11+Tbytes]=lbe>>8;
    str[12+Tbytes]=lbe;
    BN_bn2bin(e,str+13+Tbytes);

    return(13+Tbytes+lBe);
}


int get_priv_packet(
                    unsigned char * str,
                    BIGNUM * n,
                    BIGNUM * e,
                    BIGNUM * d,
                    BIGNUM * p,
                    BIGNUM * q,
                    BIGNUM * u
                   )
{

    //calculated values
    unsigned int lBd=(unsigned int)BN_num_bytes(d);
    unsigned int lbd=(unsigned int)BN_num_bits(d);

    unsigned int lBp=(unsigned int)BN_num_bytes(p);
    unsigned int lbp=(unsigned int)BN_num_bits(p);

    unsigned int lBq=(unsigned int)BN_num_bytes(q);
    unsigned int lbq=(unsigned int)BN_num_bits(q);

    unsigned int lBu=(unsigned int)BN_num_bytes(u);
    unsigned int lbu=(unsigned int)BN_num_bits(u);

    //public key
    int publen = get_pub_packet(str,n, e, d,1);

    //header (rewrite public key header)
    str[0]=0x95;
    str[1]=((8+lBu+lBq+lBp+lBd+publen)>>8);
    str[2]=(8+lBu+lBq+lBp+lBd+publen);

    // string to key usage (clear)
    str[publen]=0;

    // algo specific fields d
    //MPI d
    str[1+publen]=lbd>>8;
    str[2+publen]=lbd;
    BN_bn2bin(d,str+3+publen);

    // algo specific fields p (p<q)
    //MPI p
    str[3+lBd+publen]=lbp>>8;
    str[4+lBd+publen]=lbp;
    BN_bn2bin(p,str+5+lBd+publen);

    // algo specific fields q (p<q)
    //MPI q
    str[5+lBp+lBd+publen]=lbq>>8;
    str[6+lBp+lBd+publen]=lbq;
    BN_bn2bin(q,str+7+lBp+lBd+publen);

    // algo specific fields u (inverse of p, mod q)
    //MPI u
    str[7+lBq+lBp+lBd+publen]=lbu>>8;
    str[8+lBq+lBp+lBd+publen]=lbu;
    BN_bn2bin(u,str+9+lBq+lBp+lBd+publen);

    //checksum (application SHOULD NOT use), so I'm not RFC-complient
    //and I'm not generate it!

    unsigned int ck=0;
    unsigned int t;
    for(t=1+publen; t<9+lBu+lBq+lBp+lBd+publen; t++)
    {
        ck+=str[t];
        ck%=65536;
    }

    str[9+lBu+lBq+lBp+lBd+publen]=ck>>8;
    str[10+lBu+lBq+lBp+lBd+publen]=ck;

    return(11+lBu+lBq+lBp+lBd+publen);


}


void write_keys(
                BIGNUM * n,
                BIGNUM * e,
                BIGNUM * d,
                BIGNUM * p,
                BIGNUM * q,
                BIGNUM * u,
                char * prefix,
                unsigned char * hash,
                double score
               )
{
    //public key
    unsigned char pub[maxpublen];
    int publen = get_pub_packet(pub,n,e,d,1);

    //priv
    unsigned char priv[maxprivlen];
    int privlen = get_priv_packet(priv,n,e,d,p,q,u);


    // write

    char filename[1024];

    sprintf(filename,"%s%07.3f-%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X.gpg",prefix,score,hash[0],hash[1],hash[2],hash[3],hash[4],hash[5],hash[6],hash[7],hash[8],hash[9],hash[10],hash[11],hash[12],hash[13],hash[14],hash[15],hash[16],hash[17],hash[18],hash[19]);

    FILE * f = fopen(filename,"w");

    if(f)
    {
        fwrite( pub, 1, publen, f);
        fwrite( priv, 1, privlen, f);

        // fakeuid 
        fputc( 0xB4, f);
        fputc( 0x09, f);
        fprintf(f,"delete_me");
        fclose(f);
    }
}

