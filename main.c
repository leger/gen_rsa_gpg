/* Copyright (c) 2012, Jean-Benoist Leger <leger@jblg.fr>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include "all.h"

int main(int argc, char ** argv)
{
    if(argc<2)
    {
        fprintf(stderr,"Fuck: need one prefix to write files (prefix can contain a /)\n");
        abort();
    }

    int publen;
    unsigned char hash[20];
    unsigned char hash_best[20] = {0};
    unsigned char pub[maxpublen];
    char str_hash[50];
    str_hash[0]='\0';
    unsigned char tid[]=TARGET_ID;
    unsigned char tfpr[]=TARGET_FPR;

    double prob_looking[]=LOOKING;
    unsigned int confuse[16][16]={  CONFUSE_0,
                                    CONFUSE_1,
                                    CONFUSE_2,
                                    CONFUSE_3,
                                    CONFUSE_4,
                                    CONFUSE_5,
                                    CONFUSE_6,
                                    CONFUSE_7,
                                    CONFUSE_8,
                                    CONFUSE_9,
                                    CONFUSE_A,
                                    CONFUSE_B,
                                    CONFUSE_C,
                                    CONFUSE_D,
                                    CONFUSE_E,
                                    CONFUSE_F};
    char str_hash_ref[50];
    str_hash_ref[0]='\0';
    sprintf(str_hash_ref,"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",tfpr[0],tfpr[1],tfpr[2],tfpr[3],tfpr[4],tfpr[5],tfpr[6],tfpr[7],tfpr[8],tfpr[9],tfpr[10],tfpr[11],tfpr[12],tfpr[13],tfpr[14],tfpr[15]);


    unsigned long int nb=0;
    time_t time_start = time(NULL);
    time_t time_now;
    
    int i;

    fprintf(stderr,"Target: ");
    for(i=0;i<16;i+=2)
        fprintf(stderr,"%02X%02X ",tfpr[i],tfpr[i+1]);
    for(i=0;i<4;i+=2)
        fprintf(stderr,"%02X%02X ",tid[i],tid[i+1]);
    fprintf(stderr,"\n");
    
   
    double score_best = 1e20;

    while(1)
    {
        BN_CTX * ctx = BN_CTX_new();
        BN_CTX_start(ctx);
        RSA *prsa = RSA_new();

        BIGNUM * n = BN_CTX_get(ctx);
        BIGNUM * e = BN_CTX_get(ctx);
        BIGNUM * d = BN_CTX_get(ctx);
        BIGNUM * p = BN_CTX_get(ctx);
        BIGNUM * q = BN_CTX_get(ctx);
        BIGNUM * tmp = BN_CTX_get(ctx);
        
        BN_set_word(e, RSA_F4);

        if(!RSA_generate_key_ex(prsa, Tbits, e, NULL))
            error("OpenSSL ne veut pas generer de clef");

        RSA_get0_key(prsa, (const BIGNUM **)&n, (const BIGNUM **)&e, (const BIGNUM **)&d);
        RSA_get0_factors(prsa, (const BIGNUM **)&p, (const BIGNUM **)&q);

        BIGNUM * p1 = BN_CTX_get(ctx);
        BIGNUM * q1 = BN_CTX_get(ctx);
        BIGNUM * p1q1 = BN_CTX_get(ctx);

        BN_sub(p1, p, BN_value_one());
        BN_sub(q1, q, BN_value_one());
        BN_mul(p1q1,p1,q1,ctx);

        BIGNUM * u = BN_CTX_get(ctx);
        
        if(BN_cmp(p,q)>0)
        {
            BN_copy(tmp,p);
            BN_copy(p,q);
            BN_copy(q,tmp);
        }

        BN_mod_inverse(u,p,q,ctx);

        printf("n %d : ",BN_num_bits(n));
        BN_print_fp(stdout,n);
        printf("\n");
        
        printf("e %d : ",BN_num_bits(e));
        BN_print_fp(stdout,e);
        printf("\n");

        printf("p %d : ",BN_num_bits(p));
        BN_print_fp(stdout,p);
        printf("\n");
        
        printf("q %d : ",BN_num_bits(q));
        BN_print_fp(stdout,q);
        printf("\n");
        
        printf("u %d : ",BN_num_bits(u));
        BN_print_fp(stdout,u);
        printf("\n");





        publen = get_pub_packet(pub,n,e,d,1);


        BIGNUM * test = BN_CTX_get(ctx);
        nb++;
        while(nb%RESTART_FROM_KEY!=0)
        {


            publen = get_pub_packet(pub,n,e,d,0);

            SHA1(pub,publen,hash);
            nb++;

            /* key with good id ? */
            if(
                    (hash[16]==tid[0])
                    &&
                    (hash[17]==tid[1])
                    &&
                    (hash[18]==tid[2])
                    &&
                    (hash[19]==tid[3])
              )
            {
                /* Yeah, one */
                BN_gcd(test, p1q1, e,ctx);
                if(BN_is_one(test))
                {
                    /* This key is valid */
                    /* Score computation */
                    sprintf(str_hash,"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",hash[0],hash[1],hash[2],hash[3],hash[4],hash[5],hash[6],hash[7],hash[8],hash[9],hash[10],hash[11],hash[12],hash[13],hash[14],hash[15]);
                    double score=0;

                    for(i=0;i<32;i++)
                    {
                        unsigned int num=0;
                        if('0'<=str_hash[i] && str_hash[i]<='9')
                        {
                            num=str_hash[i]-'0';
                        }
                        else
                        {
                            num=str_hash[i]-'A'+10;
                        }

                        unsigned int num_ref=0;
                        if('0'<=str_hash_ref[i] && str_hash_ref[i]<='9')
                        {
                            num_ref=str_hash_ref[i]-'0';
                        }
                        else
                        {
                            num_ref=str_hash_ref[i]-'A'+10;
                        }

                        score-=log(1-prob_looking[i]+prob_looking[i]*confuse[num][num_ref]*1.0/1e6);
                    }

                    if(score<score_best)
                    {
                        for(i=0;i<20;i++)
                        {
                            hash_best[i]=hash[i];
                            score_best=score;

                        }
                    }
                    BN_mod_inverse(d,e,p1q1,ctx);
                    write_keys(n,e,d,p,q,u,argv[1],hash,score);

                }
                /* else FUCK ! */
            }

            if(nb%DISPLAY_EACH==0)
            {



                /* Here we display state */
                time_now=time(NULL);
                fprintf(stderr,"\rBest:  ");
                for(i=0;i<20;i+=2)
                    fprintf(stderr," %02X%02X",hash_best[i],hash_best[i+1]);
                fprintf(stderr,", Score: %g",score_best);
                fprintf(stderr,", Hashs: %lluM",nb/((unsigned long long)1e6));
                unsigned int tps = (time_now-time_start)>0 ? (time_now-time_start) : 1;
                fprintf(stderr,", Rate: %lluk/s",nb/((unsigned long long)(1e3*tps)));
                fprintf(stderr,", Time: %lus ",time_now-time_start);



            }

               
                
            



            BN_add_word(e, 2L);
        }


        BN_CTX_end(ctx);
        BN_CTX_free(ctx);

        RSA_free(prsa);
    }

    return(0);
}
    

    
